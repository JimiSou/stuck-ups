<!DOCTYPE html>
<?php
$time_start = microtime(true);
include ('safe.php');
include ('../config.php');
?>

<html lang="cs">
<head>
<title>Web administration - <?php vypis(getSetting("title",1));?></title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
<script language="Javascript" type="text/javascript" src="edit_area/edit_area_full.js"></script>
	<script language="Javascript" type="text/javascript">
		// initialisation
		editAreaLoader.init({
			id: "htmlhighlight"	// id of the textarea to transform		
			,start_highlight: true	// if start with highlight
			,allow_resize: "both"
			,allow_toggle: true
			,word_wrap: true
			,language: "en"
			,syntax: "html"
		});
		</script>
</head>
<body>
<header><h1>Web Administration<br />-<a href="http://www.stuck-ups.com" target="_blank"><?php vypis(getSetting("title",1));?></a>-</h1>
<navigation>
<ul class="admin_menu">
<li><a href="administrace.php?admin=pages">Pages</a></li>
<li><a href="administrace.php?admin=projects">Projects</a></li>
<li><a href="administrace.php?admin=galleries">Galleries</a></li>
<li><a href="administrace.php?admin=webdesigns">Web Designs</a></li>
<li><a href="administrace.php?admin=upload">Upload</a></li>
<li><a href="administrace.php?admin=settings">Settings</a></li>
<li><a href="administrace.php?admin=accounts">Accounts</a></li>

</ul>
<a href="../index.php" target="_blank" >Show page</a>________________<?php $datum = StrFTime("%d/%m/%Y %H:%M", Time());
echo($datum); ?>_____________<a href="administrace.php?admin=profile">Profile</a>,<a href="logout.php" >Logout</a>
</navigation>
</header>
<section class="admin_section">
<?php

if (isset($_GET["admin"]))
{
$admin = $_GET["admin"];
switch($admin) 
{
      case 'pages':
         include "pageslist.php";
         break;
      case 'settings':
         include "settings.php";
         break;
      case 'index':
         echo "Welcome to The Administration!";
         break;
      case 'edit':
         include "edit.php";
         break;
      case 'delete':
         include "delete.php";
         break;
      case 'new':
         include "new.php";
         break;
      case 'accounts':
         include "accounts.php";
         break;
  	  case 'profile':
         include "profile.php";
         break;
      case 'upload':
         include "upload.php";
         break;
      case 'galleries':
         include "gallery.php";
         break;
         case 'webdesigns':
         include "webdesigns.php";
         break;
      case 'designsupload':
         include "designsupload.php";
         break;
      case 'projects':
         include "projects.php";
         break;
      case 'projectinfo':
         include "projectinfo.php";
         break;
      //...
      default:
         echo "error!";
         break;

}
}
?>
</section>
<footer><?php echo date("Y"); ?> © Stuck-ups Web Administration<br><?php echo 'Total execution time (s): ' . (microtime(true) - $time_start); ?>	</footer>
</body>
</html>