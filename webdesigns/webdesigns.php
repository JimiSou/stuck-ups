<?php include "../config.php"; 
?>

<!DOCTYPE html>

<html lang="cs">

<head>
   <meta name="author" content="Stuck-ups" >
   <meta name="description" content="Náhledy pracovních verzí návrhů designů a webů Stuck-ups Webdesigns" >
   <title>Koncepty > Web Designs > Stuck-ups prod.</title>
   <base href="http://www.stuck-ups.com" />
   <meta charset="utf-8">      
   <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
   <link rel="stylesheet" type="text/css" href="/webdesigns/style.css">
   <link rel="stylesheet" type="text/css" href="/css/font-awesome.css">
   
   
</head>

<body>


<header>
<img src="http://www.stuck-ups.com/images/WEBDESIGNS_KONCEPTY.png" alt="header">
<p>Zde můžete nahlédnout do probíhajících prací na webových projektech.</p>
</header>
<section>
<?php 

$data = getAllWebdesigns();


while($zaznam=$data->fetch_assoc() ) //pro kaûd˝ z·znam
{               
	echo '<div class="box">';
    echo  '<h2>'.htmlspecialchars($zaznam["title"],ENT_QUOTES).'</h2>';
    echo '<img src="http://www.stuck-ups.com/webdesigns/logos/'.$zaznam["logo_path"].'" alt="'.htmlspecialchars($zaznam["title"],ENT_QUOTES).' logo"><br>';
    echo '<a href="http://www.stuck-ups.com/webdesigns/designs.php?dir='.$zaznam["dir_path"].'" title="Návrhy"><i class="fa fa-file-image-o fa-4x"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;';
    echo '<a href="http://www.stuck-ups.com/webdesigns/sites/'.$zaznam["demo_path"].'" title="Náhled"><i class="fa fa-desktop fa-4x"></i></a>';
  	echo '</div>';
}
  





 ?>
<section>
<footer><?php echo date("Y"); ?> © <a href="http://www.stuck-ups.com/<?php echo $lang;?>/web-designs">Stuck-ups Web Designs</a>&nbsp;&nbsp;<a href="http://stuck-ups.com/admin" class="admin fa fa-diamond" target="_blank"></a></footer>
</body>
</html>