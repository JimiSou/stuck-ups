<?php include "../config.php"; 

function listImages ($path)
{
	if (is_dir($path)) 
	{
		$allfiles = scandir($path);
		
        	
			$excludedfiles = array('..', '.');
			$files = array_diff($allfiles,$excludedfiles);
			
			foreach ($files as $value) 
   			{ 
      			if (!is_dir($path.$value))
      			{
      			$pathparts = pathinfo($value);
      			$extension = $pathparts['extension'];
      			$name = $pathparts['filename'];
      			echo '<img src="http://www.stuck-ups.com/webdesigns/'.$path.'/'.$value.'"><br><br>';
      			}
   			} 
   			
   	   
    }
}	


?>

<!DOCTYPE html>

<html lang="cs">

<head>
   <meta name="author" content="Stuck-ups" >
   <meta name="description" content="Náhledy pracovních verzí návrhů designů a webů Stuck-ups Webdesigns" >
   <title>Návrhy [<?php echo $_GET["dir"]; ?>] > Stuck-ups prod.</title>
   <base href="http://www.stuck-ups.com" />
   <meta charset="utf-8">      
   <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
   <link rel="stylesheet" type="text/css" href="/webdesigns/style.css">
   <link rel="stylesheet" type="text/css" href="/css/font-awesome.css">
   
   
</head>

<body>


<header>
<h1>Návrhy Stuck-ups Web Designs </h1>
<p>Různé verze designů k projektu [<?php echo $_GET["dir"]; ?>]</p>
</header>
<section>
<?php 

echo listImages('designs/'.$_GET["dir"]);
  





 ?>
<section>
<footer><?php echo date("Y"); ?> © <a href="http://www.stuck-ups.com/<?php echo $lang;?>/web-designs">Stuck-ups Web Designs</a>&nbsp;&nbsp;<a href="http://stuck-ups.com/admin" class="admin fa fa-diamond" target="_blank"></a></footer>
</body>
</html>