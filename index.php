<?php include "config.php";

session_start();

if (isset($_GET['lang']))
{
	$_SESSION['lang']=$_GET['lang'];
	$lang=$_GET['lang'];
}
if (isset($_SESSION['lang']))
{
	$lang=$_SESSION['lang'];
}
if (!isset($_GET['lang'])&&!isset($_SESSION['lang']))
{
	$lang=substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
}
if ($lang=='cs')
{
	$data=GetPageData($_GET['page'],'cs');

}
else
{
	$data=GetPageData($_GET['page'],'en');

}
?>


<!DOCTYPE html>

<html lang="<?php echo $lang; ?>">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="author" content="Stuck-ups" >
   <meta name="description" content="<?php echo $data["description"]; ?>" >
   <title><?php echo $data["title"]; ?> > Stuck-ups Prod.</title>
   <base href="https://www.stuck-ups.com" />
   <meta name="google-site-verification" content="pgHkkYfFt3QQ2Rs7umTdcTa1afKW_n50NobZY2gyyd4" />
   <meta charset="utf-8">
   <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
   <link rel="stylesheet" type="text/css" href="https://stuck-ups.com/css/style.php?page=<?php echo $_GET['page']; ?>" media="screen">
   <link rel="stylesheet" type="text/css" href="https://stuck-ups.com/css/font-awesome.css">
   <link rel="stylesheet" type="text/css" href="https://stuck-ups.com/css/slick.css">
  	<link rel="stylesheet" type="text/css" href="https://stuck-ups.com/css/slick-theme.css">
   <meta property="og:title" content="<?php echo $data["title"]; ?> > Stuck-ups Prod.">
	<meta property="og:image" content="https://www.stuck-ups.com/images/chameleon.png">
	<meta property="og:description" content="<?php echo $data["description"]; ?>">

</head>

<body>
  <div id="wrapper">
    <header>
        <a class="logo" href="https://www.stuck-ups.com" title="Homepage"><img  src="./images/chameleon_white.png" alt="Stuck-ups production"></a>&nbsp;&nbsp;<h1> <?php echo $data["title"]; ?></h1>

    </header>

      <nav><?php vypis(menu($lang)); ?></nav>
<?php echo $data["content"]; ?>



  </div>
     <footer><?php echo date("Y"); ?> © <a href="https://www.stuck-ups.com/<?php echo $lang;?>/web-designs">Stuck-ups Web Designs</a>&nbsp;&nbsp;<a href="https://stuck-ups.com/admin" class="admin fa fa-diamond" target="_blank"></a><br><br><div id="sidebox"><a href="https://stuck-ups.com/<?php if ($lang=='cs'){echo 'cs/kontakty';}else {echo 'en/contacts';} ?>"><img class="icon" src="images/email.png" alt="Contact"></a><br><a href="https://soundcloud.com/stuck-ups-production" target="_blank" ><img class="icon" src="images/soundcloud.png" alt="soundcloud"></a><img class="click" src="images/click.png" alt="Social networks"><a href="https://www.youtube.com/channel/UCJenxmzfoqRy6MwGCWahS8Q" target="_blank"><img class="icon" src="images/youtube.png" alt="youtube"></a><br><a href="https://www.facebook.com/stuckups.production/" target="_blank"><img class="icon" src="images/facebook.png" alt="facebook"></a></div><br><div id="lang"><a href="<?php echo langswitch($_GET['page'],'en'); ?>"><img class="flag" src="images/en.png" alt="English"></a><a href="<?php echo langswitch($_GET['page'],'cs'); ?>"><img class="flag" src="images/cz.png" alt="Čeština"></a></div></footer>




<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://stuck-ups.com/scripts/slick.min.js"></script>
<script type="text/javascript" src="https://stuck-ups.com/scripts/slick_init.js"></script>

</body>

</html>
