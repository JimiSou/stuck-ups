             
<?php 
include ('access.php'); // připojení souboru s přihlašovacími údaji


function uploadFile ($targetDir, $extensions, $mode)
{
	
	if ($mode == 'multiple')
	{
		for ($j=0; $j < count($_FILES["fileToUpload"]['name']); $j++)
		{
			$file_name = basename($_FILES["fileToUpload"]['name']["$j"]);
			$target_file_path = $targetDir . $file_name;
			$uploadOk = 1;
			$extensionOk = 1;
			$fileExtension = pathinfo($file_name,PATHINFO_EXTENSION);
			// Check if file already exists
			if (file_exists($target_file_path)) 
			{
    			echo "Sorry, file already exists.";
    			$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]['size']["$j"] > 2000000) 
			{
    			echo "Sorry, your file is too large.";
    			$uploadOk = 0;
			}
			// Allow certain file formats
			foreach ($extensions as &$hodnota) 
			{
    			if ($hodnota == $fileExtension)
    			{
    				$uploadOk = 1;
    				$extensionOk = 1;
    				break;
    			}   
    			else
    			{
    				$extensionOk = 0;
    				$uploadOk=0;
    			} 
			}
			if($uploadOk == 0 && $extensionOk == 0)
			{
				echo "Sorry, files with this extension are not allowed!";
			}
			if ($uploadOk == 0) // Check if $uploadOk is set to 0 by an error
			{
    			echo "Sorry, your file was not uploaded.";
			} 
			else // if everything is ok, try to upload file
			{
    			if (move_uploaded_file($_FILES["fileToUpload"]['tmp_name']["$j"], $target_file_path)) 
    			{
        			echo "The file ". basename( $_FILES["fileToUpload"]['name']["$j"]). " has been uploaded.";
    			} 
    			else 
    			{
        			echo "Sorry, there was an error uploading your file.";
    			}
			}
		}
	}
	
	if ($mode == 'single')
	{
		$file_name = basename($_FILES["fileToUpload"]['name']);
		$target_file_path = $targetDir . $file_name;
		$uploadOk = 1;
		$extensionOk = 1;
		$fileExtension = pathinfo($file_name,PATHINFO_EXTENSION);
// Check if file already exists
		if (file_exists($target_file_path)) 
		{
    		echo "Sorry, file already exists.";
    		$uploadOk = 0;
		}
// Check file size
		if ($_FILES["fileToUpload"]['size'] > 50000000) 
		{
    		echo "Sorry, your file is too large.";
    		$uploadOk = 0;
		}
// Allow certain file formats
		foreach ($extensions as &$hodnota) 
		{
    		if ($hodnota == $fileExtension)
    		{
    			$uploadOk = 1;
    			$extensionOk = 1;
    			break;
    		}   
    		else
    		{
    			$extensionOk = 0;
    			$uploadOk=0;
    		} 
		}
		if($uploadOk == 0 && $extensionOk == 0)
		{
			echo "Sorry, files with this extension are not allowed!";
		}
		if ($uploadOk == 0) // Check if $uploadOk is set to 0 by an error
		{
    		echo "Sorry, your file was not uploaded.";
		} 
		else // if everything is ok, try to upload file
		{
    		if (move_uploaded_file($_FILES["fileToUpload"]['tmp_name'], $target_file_path)) 
    		{
        		echo "The file ". $file_name. " has been uploaded.";
    		}	 
    		else 
    		{
        		echo "Sorry, there was an error uploading your file.";
    		}
		}
	}
	
}



function getSetting($type, $id) //získání hodnoty nastavení dle typu ZMENIT NA ZISKANI CELEHO POLE A V INTERFACE CTENI LIBOVOLNYCH HODNOT
{
	$dotaz="SELECT `".$type."` FROM  `Settings` WHERE Settings.id =  ".$id.";";     //Dotaz pro získání hodnoty, kde se typ=vstupnímu argumentu
	$vysledek=db_conn($dotaz);       //vykonání dotazu
	if ($vysledek) 
	{           //pokud dotaz probehl
		while($zaznam = $vysledek->fetch_assoc()) 
		{
		return $zaznam[$type];    //navrácení hodnoty			
		}
		
	
	}
	else
	{ 
		echo ("Dotaz na databazi se nezdaril.");    //výpis chybové hlášky pokud se dotaz nezdařil
		
		   
	}  
}


function langswitch ($id, $lang)
{
	if ($lang == 'cs')
	{
		$dotazcs="SELECT  `title`,`id` FROM  `Pages` WHERE `hide`= 0 AND `projectID` = 0 ORDER BY ordinaryNo; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
 		$vysledekcs=db_conn($dotazcs);           //provedenÌ dotazu
 		if ($vysledekcs) 
 		{
 			while($zaznam=$vysledekcs->fetch_assoc() ) 
   			{               
				if ($zaznam["id"] == $id)
				{
					$cstitle = textUrl($zaznam["title"]);
					return 'http://www.stuck-ups.com/cs/'.$cstitle;
				}
 			}
 		
 		}
 	}	
 		
 	else
 	{	
 		$dotazen="SELECT  `title`,`id` FROM  `PagesEN` WHERE `hide`= 0 AND `projectID` = 0 ORDER BY ordinaryNo; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
 		$vysledeken=db_conn($dotazen);           //provedenÌ dotazu
 		
 		if ($vysledeken) 
 		{
 		
 			while($zaznam2=$vysledeken->fetch_assoc() ) 
   			{               
				if ($zaznam2["id"] == $id)
				{
					$entitle = textUrl($zaznam2["title"]);
					return 'http://www.stuck-ups.com/en/'.$entitle;
				}
 			}
 		
 		}
 	}
 		
}


function menu ($lang)      //v˝pis menu
{
 	if ($lang=='cs')
 	{
 	$dotaz="SELECT  `title`,`id` FROM  `Pages` WHERE `hide`= 0 AND `projectID` = 0 ORDER BY ordinaryNo; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
 	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	if ($vysledek) 
 	{
 		
 		echo '<ul>';
   		while($zaznam=$vysledek->fetch_assoc() ) 
   		{               //pro kaûd˝ z·znam
			$url= textUrl($zaznam["title"]);
			
			echo '<li><a href="http://www.stuck-ups.com/'.$lang.'/'.$url.'">'.$zaznam["title"].'</a></li>';  //v˝pis vyescapovanÈho HTML odkazu
 
  		}
  		echo '</ul>';
  		
 	}
 	}
 	else
 	{
 	$dotaz="SELECT  `title`,`id` FROM  `PagesEN` WHERE `hide`= 0 AND `projectID` = 0 ORDER BY ordinaryNo; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
 	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	if ($vysledek) 
 	{
 		
 		echo '<ul>';
   		while($zaznam=$vysledek->fetch_assoc() ) 
   		{               //pro kaûd˝ z·znam
			$url= textUrl($zaznam["title"]);
			echo '<li><a href="http://www.stuck-ups.com/'.$lang.'/'.$url.'">'.$zaznam["title"].'</a></li>';  //v˝pis vyescapovanÈho HTML odkazu
 
  		}
  		echo '</ul>';
  		
 	}
 	}
 
}

function projectMenu ($projectID,$lang)
{
if ($lang=='cs')
{
$dotaz="SELECT Pages.title, Pages.hide, Projects.title AS project, Pages.ordinaryNo FROM `Pages` LEFT JOIN `Projects` ON Pages.projectID=Projects.id WHERE Pages.hide = 0 AND Pages.projectID = ".$projectID." ORDER BY Pages.ordinaryNo ASC;";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
 	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	if ($vysledek) 
 	{
 		echo '<ul>';
                $count=0;
   		while($zaznam=$vysledek->fetch_assoc() ) 
   		{               //pro kaûd˝ z·znam
                    $count++;
                    $projectname=textUrl($zaznam["project"]);
                    /*echo '<li><a href="project.php?project='.$projectID.'&page='.$count.'">'.$zaznam["title"].'</a></li>';  //v˝pis vyescapovanÈho HTML odkazu*/
					 echo '<li><a href="http://www.stuck-ups.com/'.$lang.'/projekty/'.$projectname.'/'.$count.'">'.$zaznam["title"].'</a></li>';  //v˝pis vyescapovanÈho HTML odkazu
  		}
  		echo '</ul>';
  		
 	}
 }
 else
 {
$dotaz="SELECT PagesEN.title, PagesEN.hide, Projects.title AS project, PagesEN.ordinaryNo FROM `PagesEN` LEFT JOIN `Projects` ON PagesEN.projectID=Projects.id WHERE PagesEN.hide = 0 AND PagesEN.projectID = ".$projectID." ORDER BY PagesEN.ordinaryNo ASC;";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
 	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 	if ($vysledek) 
 	{
 		echo '<ul>';
                $count=0;
   		while($zaznam=$vysledek->fetch_assoc() ) 
   		{               //pro kaûd˝ z·znam
                    $count++;
    $projectname=textUrl($zaznam["project"]);
                    /*echo '<li><a href="project.php?project='.$projectID.'&page='.$count.'">'.$zaznam["title"].'</a></li>';  //v˝pis vyescapovanÈho HTML odkazu*/
					 echo '<li><a href="http://www.stuck-ups.com/'.$lang.'/projects/'.$projectname.'/'.$count.'">'.$zaznam["title"].'</a></li>';  //v˝pis vyescapovanÈho HTML odkazu
  		}
  		echo '</ul>';
  		
 	}
 }
}
 
function getPageData($pageid,$lang)   //funkce pro zÌsk·nÌ dat o aktu·lnÌ str·nce
{
	
    if ($lang=='cs')
    {
    
    	if ($pageid != null)
		{
 			$dotaz="SELECT * FROM  `Pages` WHERE `id` = '".$pageid."';";            //dotaz na vöechny str·nky
			$vysledek=db_conn($dotaz);
			if ($vysledek)
			{
				
				return $vysledek->fetch_assoc(); //vr·cenÌ pole(celÈho z·znamu)
				
			}
			else 
			{
				echo "CHYBA!";
			}
		}
		else
		{
 			$dotaz="SELECT * FROM  `Pages` WHERE `id` = '1';";            //dotaz na vöechny str·nky
			$vysledek=db_conn($dotaz);
			if ($vysledek)
			{
				
				return $vysledek->fetch_assoc(); //vr·cenÌ pole(celÈho z·znamu)
				
			}
 
 		}
 	}
 	else
    {
    
    	if ($pageid != null)
		{
 			$dotaz="SELECT * FROM  `PagesEN` WHERE `id` = '".$pageid."';";            //dotaz na vöechny str·nky
			$vysledek=db_conn($dotaz);
			if ($vysledek)
			{
				
				return $vysledek->fetch_assoc(); //vr·cenÌ pole(celÈho z·znamu)
				
			}
			else 
			{
				echo "CHYBA!";
			}
		}
		else
		{
 			$dotaz="SELECT * FROM  `PagesEN` WHERE `id` = '1';";            //dotaz na vöechny str·nky
			$vysledek=db_conn($dotaz);
			if ($vysledek)
			{
				
				return $vysledek->fetch_assoc(); //vr·cenÌ pole(celÈho z·znamu)
				
			}
 
 		}
 	}
     
	
 }
 
function getProjectData ($id,$mode)
{
	if ($mode=='basic')
    {
    	$dotaz="SELECT  * FROM  `Projects` WHERE `id`= '".$id."'; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
			
 			return $vysledek->fetch_assoc();
 			
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}
         
    }
     
	
 }
  
function getProjectDataAll ()
{
		$dotaz="SELECT  * FROM  `Projects`; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
			
 			return $vysledek;
 			
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}
}

function vypis($text)     //vyescapovany vypis do HTML
{
	echo htmlspecialchars($text,ENT_QUOTES);
}

 
function pageHead()
{
	$dotaz="SELECT  * FROM  `Settings` WHERE Settings.id = 1; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 	if ($vysledek) //pokud je dotaz proveden
 	{
 		
 		while($zaznam=$vysledek->fetch_assoc()) 
		{
   				echo '<meta name="author" content="'.htmlspecialchars($zaznam["author"],ENT_QUOTES).'" >';
   				echo '<meta name="description" content="'.htmlspecialchars($zaznam["description"],ENT_QUOTES).'" >'; 
   				echo '<meta name="keywords" content="'.htmlspecialchars($zaznam["keywords"],ENT_QUOTES).'" >';  
   			
    			echo '<title>'.htmlspecialchars($zaznam["title"],ENT_QUOTES).'</title>';
    		
    			echo '<meta charset="'.htmlspecialchars($zaznam["charset"],ENT_QUOTES).'">';
    		       
 		}
 		
 	}
 	else //pokud ne vypíšeme error
 	{
 		echo 'Error occured!';
 		
 	}
 }
 
function getProfile ($login)
{
	$dotaz="SELECT  * FROM  `Users` WHERE `login`= '".$login."'; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
	$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 	if ($vysledek) //pokud je dotaz proveden
 	{

 		while($zaznam=$vysledek->fetch_assoc() ) //pro každý záznam
    	{ 
    		echo 'Login: <br><input name="login" value="'.$zaznam["login"].'"disabled><br>';
    		echo '<form method="post" action="administrace.php?admin=edit"><input type="hidden" name="user" value="'.$_SESSION["id"].'"><button type="submit">Change password</button></form>';
    	}
 		
 	}
 	else
 	{
 		echo 'Error occured!';
 		
 	}
 }
 
 function getGalleryData ($id,$mode)
 {
 if ($mode == 'gallery')
 {
 $dotaz="SELECT * FROM  `Galleries` WHERE `id` = '".$id."';";            //dotaz na vöechny str·nky
		$vysledek=db_conn($dotaz);
		if ($vysledek)
		{
			return $vysledek->fetch_assoc(); //vr·cenÌ pole(celÈho z·znamu)
			
		}
		else 
		{
			echo "CHYBA!";
		}
	}
	if ($mode == 'images')
 {
 $dotaz="SELECT Galleries.id as ID, Galleries.name as Nazev, Images.ordinaryNo as Poradi, Images.id as IID, Images.name as Soubor, Images.path as Cesta, Images.galleryID as GalerieID, Images.extension as Koncovka, Images.description as Popis
FROM  Galleries INNER JOIN Images
ON Galleries.id = Images.galleryID WHERE Galleries.id = ".$id." ORDER BY Images.ordinaryNo ASC;";
$vysledek=db_conn($dotaz);
		if ($vysledek)
		{
			return $vysledek; //vr·cenÌ pole(celÈho z·znamu)
			
		}
		else 
		{
			echo "CHYBA!";
		}
 }
 }
 
 function deleteFile ($path)
{
	if (file_exists($path)) 
	{
        unlink($path);
        echo 'File has been deleted!';
    }
    else
    {
    	echo 'File does not extist!';
    }
        
}


function getProjectInfo ($id)
{
$dotaz="SELECT  * FROM  `ProjectInfo` WHERE `projectID`= '".$id."' ORDER BY `ordinaryNo` ASC; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
			
 			return $vysledek;
 			
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}
}


function getProjectPages ($id)
{
$dotaz="SELECT  * FROM  `Pages` WHERE `projectID`= '".$id."' ORDER BY `ordinaryNo`; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
			
 			return $vysledek;
 			
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}

} 

function getProjectPagesAll ()
{
$dotaz="SELECT  * FROM  `Pages` WHERE `projectID`>0 ORDER BY `projectID` AND `ordinaryNo`; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
			
 			return $vysledek;
 			
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}

} 

function getProjectPage ($projectid, $page,$lang)
{
  
  if ($lang=='cs')
  {
  $dotaz="SELECT  * FROM  `Pages` WHERE `projectID`= '".$projectid."' AND `ordinaryNo`= '".$page."'; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
 			return $vysledek;
            
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}  
 	}
 	else
  {
  $dotaz="SELECT  * FROM  `PagesEN` WHERE `projectID`= '".$projectid."' AND `ordinaryNo`= '".$page."'; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
 			return $vysledek;
            
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}  
 	}
}

function getProgress ($id)
{
		$dotaz="SELECT  * FROM  `ProjectProgress` WHERE `id`= ".$id."; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
 			return $vysledek->fetch_assoc();
            
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}  
 	
}

function getProjnProgAll()
{
$dotaz="SELECT *
FROM `Projects`
INNER JOIN `ProjectProgress`
ON Projects.id=ProjectProgress.id;";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
			
 			return $vysledek;
 			
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}
}


function getWebdesign ($id)
 {

 $dotaz="SELECT * FROM  `Webdesigns` WHERE `id` = ".$id.";";            //dotaz na vöechny str·nky
		$vysledek=db_conn($dotaz);
		if ($vysledek)
		{
			return $vysledek->fetch_assoc(); //vr·cenÌ pole(celÈho z·znamu)
			
		}
		else 
		{
			echo "CHYBA!";
		}
	
	
 }

function getAllWebdesigns ()
{
$dotaz="SELECT  * FROM  `Webdesigns`; ";    //zÌsk·nÌ n·zv˘ vöech str·nek a jejich id z datab·ze
		$vysledek=db_conn($dotaz);           //provedenÌ dotazu
 
 		if ($vysledek) //pokud je dotaz proveden
 		{
			
 			return $vysledek;
 			
 		}
 		else
 		{
 			echo 'Error occured!';
 			
 		}

} 




function getColors($imageFile, $numColors, $granularity = 5)  //Getting colors from the picture by JKirchartz from stackoverflow
{ 
   $granularity = max(1, abs((int)$granularity)); 
   $colors = array(); 
   $size = @getimagesize($imageFile); 
   if($size === false) 
   { 
      user_error("Unable to get image size data"); 
      return false; 
   } 
   $img = @imagecreatefromstring(file_get_contents($imageFile)); 
    

   if(!$img) 
   { 
      user_error("Unable to open image file"); 
      return false; 
   } 
   for($x = 0; $x < $size[0]; $x += $granularity) 
   { 
      for($y = 0; $y < $size[1]; $y += $granularity) 
      { 
         $thisColor = imagecolorat($img, $x, $y); 
         $rgb = imagecolorsforindex($img, $thisColor); 
         $red = round(round(($rgb['red'] / 0x33)) * 0x33); 
         $green = round(round(($rgb['green'] / 0x33)) * 0x33); 
         $blue = round(round(($rgb['blue'] / 0x33)) * 0x33); 
         $thisRGB = sprintf('%02X%02X%02X', $red, $green, $blue); 
         if(array_key_exists($thisRGB, $colors)) 
         { 
            $colors[$thisRGB]++; 
         } 
         else 
         { 
            $colors[$thisRGB] = 1; 
         } 
      } 
   } 
   arsort($colors); 
   return array_slice(array_keys($colors), 0, $numColors); 
} 

function colorDiff($rgb1,$rgb2)
{
    // do the math on each tuple
    // could use bitwise operates more efficiently but just do strings for now.
    $red1   = hexdec(substr($rgb1,0,2));
    $green1 = hexdec(substr($rgb1,2,2));
    $blue1  = hexdec(substr($rgb1,4,2));

    $red2   = hexdec(substr($rgb2,0,2));
    $green2 = hexdec(substr($rgb2,2,2));
    $blue2  = hexdec(substr($rgb2,4,2));

    return abs($red1 - $red2) + abs($green1 - $green2) + abs($blue1 - $blue2) ;

}

function colorBrightness($rgb)
{

//break up the color in its RGB components
$r = hexdec(substr($rgb,1,3));
$g = hexdec(substr($rgb,3,3));
$b = hexdec(substr($rgb,5,3));

//do simple weighted avarage
//
//(This might be overly simplistic as different colors are perceived
// differently. That is a green of 128 might be brighter than a red of 128.
// But as long as it's just about picking a white or black text color...)

if($r + $g + $b > 382)
    {
        return 'bright';
    }
else
    {
        return 'dark';
    }
}

function textUrl ($text)
{
			$prevodni_tabulka = Array(
  'ä'=>'a',
  'Ä'=>'A',
  'á'=>'a',
  'Á'=>'A',
  'à'=>'a',
  'À'=>'A',
  'ã'=>'a',
  'Ã'=>'A',
  'â'=>'a',
  'Â'=>'A',
  'č'=>'c',
  'Č'=>'C',
  'ć'=>'c',
  'Ć'=>'C',
  'ď'=>'d',
  'Ď'=>'D',
  'ě'=>'e',
  'Ě'=>'E',
  'é'=>'e',
  'É'=>'E',
  'ë'=>'e',
  'Ë'=>'E',
  'è'=>'e',
  'È'=>'E',
  'ê'=>'e',
  'Ê'=>'E',
  'í'=>'i',
  'Í'=>'I',
  'ï'=>'i',
  'Ï'=>'I',
  'ì'=>'i',
  'Ì'=>'I',
  'î'=>'i',
  'Î'=>'I',
  'ľ'=>'l',
  'Ľ'=>'L',
  'ĺ'=>'l',
  'Ĺ'=>'L',
  'ń'=>'n',
  'Ń'=>'N',
  'ň'=>'n',
  'Ň'=>'N',
  'ñ'=>'n',
  'Ñ'=>'N',
  'ó'=>'o',
  'Ó'=>'O',
  'ö'=>'o',
  'Ö'=>'O',
  'ô'=>'o',
  'Ô'=>'O',
  'ò'=>'o',
  'Ò'=>'O',
  'õ'=>'o',
  'Õ'=>'O',
  'ő'=>'o',
  'Ő'=>'O',
  'ř'=>'r',
  'Ř'=>'R',
  'ŕ'=>'r',
  'Ŕ'=>'R',
  'š'=>'s',
  'Š'=>'S',
  'ś'=>'s',
  'Ś'=>'S',
  'ť'=>'t',
  'Ť'=>'T',
  'ú'=>'u',
  'Ú'=>'U',
  'ů'=>'u',
  'Ů'=>'U',
  'ü'=>'u',
  'Ü'=>'U',
  'ù'=>'u',
  'Ù'=>'U',
  'ũ'=>'u',
  'Ũ'=>'U',
  'û'=>'u',
  'Û'=>'U',
  'ý'=>'y',
  'Ý'=>'Y',
  'ž'=>'z',
  'Ž'=>'Z',
  'ź'=>'z',
  'Ź'=>'Z'
);
			$url= strtr($text, $prevodni_tabulka);
			$url= strtolower($url);
			$re = "/[^[:alpha:][:digit:]]/"; //nahrazení speciálních znaků
            $replacement = "%2D";	//pomlčkou
			$url = preg_replace ($re, $replacement, $url); //provedení metody nahrazení
			return $url;
}

function generatePath ($lang,$project,$page,$divider)
{
	echo '<a class="fa fa-home" href="http://www.stuck-ups.com"></a> '.$divider.' ';
	if ($lang=='cs')
	{
		echo '<a href="http://www.stuck-ups.com/cs/projekty">Projekty</a> '.$divider.' ';
		echo '<a href="http://www.stuck-ups.com/cs/projekty/'.textUrl($project).'">'.$project.'</a>';
	}
	else
	{
		echo '<a href="http://www.stuck-ups.com/en/projects">Projects</a> '.$divider.' ';
		echo '<a href="http://www.stuck-ups.com/en/projects/'.textUrl($project).'">'.$project.'</a>';
	}
	echo ' '.$divider.' '.$page;

}






   ?>