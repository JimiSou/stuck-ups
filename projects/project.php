<?php include "../config.php"; 
session_start();
if (isset($_GET['lang']))
{
$_SESSION['lang']=$_GET['lang'];
}
if (isset($_SESSION['lang']))
{
$lang=$_SESSION['lang'];
}
if (!isset($_GET['lang'])&&!isset($_SESSION['lang']))
{
$lang=substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
}
if ($lang=='cs')
{
if (isset($_GET['page']))
{
$content = getProjectPage($_GET['project'],$_GET['page'],'cs');
}
else
{
$content = getProjectPage($_GET['project'],1,'cs');    
}
}
else
{
if (isset($_GET['page']))
{
$content = getProjectPage($_GET['project'],$_GET['page'],'en');
}
else
{
$content = getProjectPage($_GET['project'],1,'en');    
}
}
$content=$content->fetch_assoc();




$data=getProjectData($_GET['project'],'basic');
$info = getProjectInfo($_GET['project']);
$progress=getProgress($_GET['project']);

$string = explode( "/", $_SERVER['REQUEST_URI'], 5 );
$string = array_slice( $string, 0, 4 );
$string = implode( "/", $string );
$uri = 'http://' . $_SERVER['HTTP_HOST'] . $string;

$uri = str_replace("-", "%2D", $uri);
?>


<!DOCTYPE html>

<html lang="cs">

<head> 
   <title><?php echo $data["title"]; ?> by Stuck-ups</title>
   <meta name="author" content="Stuck-ups" >
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="http://www.stuck-ups.com/projects/" />
    <link rel="stylesheet" type="text/css" href="style.php?project=<?php echo $_GET['project']; ?>" media="screen">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="../css/slick.css">
  	<link rel="stylesheet" type="text/css" href="../css/slick-theme.css">
  	<meta property="og:title" content="<?php echo $data["title"]; ?> by Stuck-ups">
	<meta property="og:image" content="http://www.stuck-ups.com/projects/<?php echo $data["cover"]; ?>">
	<meta property="og:url" content="<?php echo $uri; ?>" />
	<meta property="og:type" content="website" />
	
</head>

<body>

    <div id="wrapper">
    <div id="mainpagebox">
<?php echo generatePath($lang,$data['title'],$content["title"],'>');?>
</div>
<header>
    <a class="logo" href="http://www.stuck-ups.com"></a>
    <h1><?php echo $data["title"]; ?></h1>
    
    
    
</header> 
    <div class="panel">
        <?php 
        echo '<img id="cover" src="'.$data["cover"].'" width="300px">';
            echo '<div id="info"><ul>';
            echo '<li>Author: <span class="bigger">'.$data["author"].'</span></li>';
            echo '<li>Year: <span class="bigger">'.$data["year"].'</span></li>';
            while ($zaznam=$info->fetch_assoc())
            {
                echo '<li>'.$zaznam["name"].': <span class="bigger">'.$zaznam["value"].'</span></li>' ;
            }
            echo '</ul></div>'; 
            
            ?>
            
    </div>
     <div id="progress">
      <a href="http://www.stuck-ups.com/progress" target="_blank">Current progress: <?php echo $progress["overall"];?>%</a>
     </div>  
     <script src="../scripts/clipboard.min.js"></script>

    <script>
    var clipboard = new Clipboard('.btn');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>
 <!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v2.5";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>    
	



 </div>
    <div id="content">    
        <aside>
        <nav>
            <?php projectMenu($data["id"],$lang); ?>
            <div id="sharebox"> <i class="fa fa-share-alt fa-2x"></i><hr>
    <div class="share"><button class="btn" data-clipboard-text="<?php echo $uri; ?>"><i class="fa fa-clipboard fa-2x"></i></button><br/><?php if ($lang == 'cs'){echo 'Zkopírovat odkaz';} else { echo 'Copy link';} ?></div> 

<div class="share">
	<!-- Your share button code -->
	<div class="fb-share-button" 
		data-href="<?php echo $uri; ?>" 
		data-layout="icon">
	</div><br/>
	Facebook
	</div>
 </div>
        </nav>
        
        </aside>
        <section>
            <?php echo $content["content"];?>
        </section>
<br class="clear" />
    </div>

    </div>   
           
<footer>
    <p><?php echo date("Y"); ?> © <a href="http://www.stuck-ups.com">Stuck-ups Web Designs</a></p>
</footer>
 
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="../scripts/slick.min.js"></script>
<script type="text/javascript" src="../scripts/slick_init.js"></script>
          
</body>

</html> 
