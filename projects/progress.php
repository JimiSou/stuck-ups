<?php
include "../config.php";
?>
<!DOCTYPE html>

<html lang="cs">

<head> 
   <title>Progress of Stuck-ups</title>
   <meta name="author" content="Stuck-ups" >
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="http://www.stuck-ups.com/projects/" />
    <link rel="stylesheet" type="text/css" href="progress_style.php?project=<?php echo $_GET['project']; ?>" media="screen">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.css">

  	<meta property="og:title" content="Progress of Stuck-ups">
	<meta property="og:image" content="http://www.stuck-ups.com/projects/<?php echo $data["cover"]; ?>">
</head>

<body>
       
    <div id="wrapper">
    <header><a class="logo" href="http://www.stuck-ups.com"><img src="../images/chameleon_white.png" alt="logo"></a><h1>PROGRESS <a href="/progress#info" class="fa fa-info-circle"></a></h1></header>
    <section>
    <?php
    $projects=getProjnProgAll();
    while ($zaznam = $projects->fetch_assoc())
  {
  if ($zaznam["overall"]<100)
  {
  echo '<div class="box" id="box'.$zaznam["id"].'"><img class="cover" src="'.$zaznam["cover"].'" alt="Cover of '.$zaznam["title"].'"><span class="percentage">'.$zaznam["title"].'</span><div class="meter">';
  
  if ($zaznam["overall"]<90)
  {
  echo '<span class="arrow'.$zaznam["id"].' fa fa-long-arrow-down fa-2x"> '.$zaznam["overall"].'%</span>';
  }
  else
  {
  echo '<span class="arrow'.$zaznam["id"].' fa fa-long-arrow-down fa-2x"> '.$zaznam["overall"].'%</span>';
  }
  
  echo '<img src="../images/progressmeter2.png"></div></div>';
  }
  }
    
    ?>
    </section>
    <div id="info">
    <h1>What's this?</h1>
    <p>This is the place, where you can check current progress of "Coming soon" projects. Overall precentage is calculated by it's parts such as individual instruments composition and recording, lyrics, graphics and so on. If you feel we're too slow at finishing your beloved project, write us a message and push it forward! </p><a href="http://www.stuck-ups.com/en/contacts">Contact us!</a>
    </div>
    </div>   
           
<footer>
    <p><?php echo date("Y"); ?> © <a href="http://www.stuck-ups.com">Stuck-ups Web Designs</a></p>
</footer>
 
</body>

</html> 
