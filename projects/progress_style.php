<?php
header("Content-type: text/css");
include "../config.php";
?>
@font-face 
{
font-family: 'exoextra'; /*a name to be used later*/
src: url('../css/fonts/Exo-ExtraLight.ttf'); /*URL to font*/
}

body
{
font-family: exoextra;
//background-image: url("../images/bg_geometric2.png");
background-color: black;
//background-size: 100%;
//background-repeat: repeat-y;
color: white;
}

a
{
color: inherit;
}

#wrapper
{
width: 95%;
margin: 0 auto;
}

header
{
color: white;
text-align: center;
}

header a
{
text-decoration: none;
}
.cover
{
max-width: 150px;
box-shadow: 2px 2px 4px #000000;
border-radius: 15px;
}

.box
{
margin-bottom: 20px;
background-color:  white;
opacity: 0.9;
border-radius: 25px;
}

.meter
{
color: black;
}

.meter img
{
width: 100%;
height:180px;
}


footer
{
text-align: center;
border-top: 2px solid grey;
margin-top: 20px;
background-color: white;
color: black;
height: 55px;
opacity: 0.9;
}

.logo img
{
vertical-align: top;
-webkit-transition: all 0.5s ease-out;
-moz-transition: all 0.5s ease-out;
-ms-transition: all 0.5s ease-out;
-o-transition: all 0.5s ease-out;
transition: all 0.5s ease-out;
width: 140px;
}
.logo img:hover
{
    transform: scale(1.2);
}

#info
{
width: 50%;
border: solid 2px white;
border-radius: 25px;
padding: 30px;
padding-top: 0px;
text-align: center;
margin: 0 auto;
}

.percentage
{
color: black;
font-size: 3em;
padding: 10px;
vertical-align: top;
}
<?php
$projects=getProjnProgAll();
    while ($zaznam = $projects->fetch_assoc())
  {
  echo '#box'.$zaznam["id"].'{border: groove 10px '.$zaznam["othercolor"].';}';
  echo '.arrow'.$zaznam["id"].' {text-align: center; position:relative;left: '.($zaznam["overall"]-0.5).'%;}';
  }
?>