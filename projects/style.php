<?php
header("Content-type: text/css");
include "../config.php";
$data=getProjectData($_GET['project'],'basic');
$background=$data["bgcolor"];
$text=$data["textcolor"];
$other=$data["othercolor"];

$bgtest=colorBrightness($background);

if($bgtest=='dark')
{
    $logo = "../images/chameleon_white.png";
    
}
else
{
    $logo = "../images/chameleon.png";
}

?>

@font-face 
{
font-family: 'exoextra'; /*a name to be used later*/
src: url('../css/fonts/Exo-ExtraLight.ttf'); /*URL to font*/
}

body 
{
background:<?php echo $background; ?>;
color:<?php echo $text; ?>;
font-family: 'exoextra';
}

hr
{
border-color: <?php echo $text; ?>;
}

#mainpagebox
{
height: 30px;
background-image: url("../images/bg_geometric2.png");
border: 1px inset <?php echo $other; ?>;
background-size: 100%;
font-size: 1.2em;
color: white;
padding: 7px;
-webkit-transition: all 0.5s ease-out;
-moz-transition: all 0.5s ease-out;
-ms-transition: all 0.5s ease-out;
-o-transition: all 0.5s ease-out;
transition: all 0.5s ease-out;
}

#mainpagebox a
{
text-decoration: none;
color: white;
}

#mainpagebox a:hover
{
transform: scale(1.2);
color: #4b947f;
}

header
{
border-bottom: 2px solid <?php echo $other; ?>;
}

.logo 
{
background: url("<?php echo $logo; ?>")center;
background-repeat: no-repeat;
display: block;
-webkit-transition: all 0.5s ease-out;
-moz-transition: all 0.5s ease-out;
-ms-transition: all 0.5s ease-out;
-o-transition: all 0.5s ease-out;
transition: all 0.5s ease-out;
}

#content
{

background-color: <?php echo $text; ?>;
color: <?php echo $background; ?>;
}

nav ul li a:hover 
{
background-color: <?php echo $background; ?>;
color: <?php echo $text; ?>;
}

nav a 
{
text-decoration: none;
padding: 5px;
display: block;
color: <?php echo $background; ?>;
-webkit-transition: all 0.5s ease-out;
-moz-transition: all 0.5s ease-out;
-ms-transition: all 0.5s ease-out;
-o-transition: all 0.5s ease-out;
transition: all 0.5s ease-out;
}
section
{
border: solid 1px <?php echo $background; ?>;
}
section a
{
color: <?php echo $background; ?>;
-webkit-transition: all 0.5s ease-out;
-moz-transition: all 0.5s ease-out;
-ms-transition: all 0.5s ease-out;
-o-transition: all 0.5s ease-out;
transition: all 0.5s ease-out;
}
section img
{
max-width: 95%;
}
.center
{
text-align: center;
}


#info ul
{
list-style: square;
}

footer
{
opacity: 0.5;
text-align: center;
}
a
{
color: <?php echo $text; ?>;
}
.clear { clear: both; }

.galleryscreen
{
text-align:center;
}
.galleryscreen img
{

max-height: 500px;
max-width: 100%;
margin: 0 auto;
}

.gallerynav
{
width:85%;
margin: 0 auto;
}

.gallerynav img
{
max-height:80px;
margin-left: 10px;
}

.textnav
{
width: 85%;
margin: 0 auto;
}

.textnav div
{
margin-right: 10px;
cursor: pointer;
}


.textscreen
{
text-align:left;
white-space: pre-line;
}

.textscreen h2
{
margin-top: 0px;
text-align: center;
}

#sharebox
{
padding: 5px;
margin-top:5px;
text-align: center;
background-color: <?php echo $background; ?>;
color: <?php echo $text; ?>;
font-size: 15px;
}
#sharebox hr
{
border-color: <?php echo $text; ?>;
}

.btn
{
margin-bottom: 5px;
border-radius: 3px;
-webkit-transition: all 0.5s ease-out;
-moz-transition: all 0.5s ease-out;
-ms-transition: all 0.5s ease-out;
-o-transition: all 0.5s ease-out;
transition: all 0.5s ease-out;
background-color: <?php echo $text; ?>;
color: <?php echo $background; ?>;
}
.btn:hover
{
transform: scale (1.5,1.5);
cursor: pointer;
}
.share
{
width: 90px;
margin: auto;
margin-bottom: 30px;
}

.fb-share-button
{
transform: scale(2.5,2.5);
position: relative;
bottom: 17px;
-webkit-transition: all 0.5s ease-out;
-moz-transition: all 0.5s ease-out;
-ms-transition: all 0.5s ease-out;
-o-transition: all 0.5s ease-out;
transition: all 0.5s ease-out;
}

.fb-share-button:hover
{
transform: scale (1.5,1.5);
}


@media screen and (min-width:700px){
body 
{
width: 700px;
margin: auto;
}

h1
{

//float:left;
margin-bottom: 0;
position: relative;
top: 15px;

}

hr
{
margin-bottom: 20px;
}

header
{
height: 80px;
margin-top: 10px;
margin-bottom: 20px;

}

.logo 
{

background-size: 100px;
width: 100px;
height: 100px;
//margin-left: 85%;
float:right;
position: relative;
bottom: 20px;
z-index: 100;

}

.logo:hover 
{
transform: scale(1.2);
cursor:pointer;
}

.panel
{
width: 100%;
height: 300px;
}

nav
{
float: left;
padding: 10px;
width: 100px;
}

nav ul
{
padding-left: 0;
list-style: none;
font-size: 1.2em;
width: 100%;
}

nav ul li 
{
margin-bottom: 5px;
}



nav a 
{
width: 100%;
}

#progress
{
height: 20px;
margin-top:10px;
text-align: center;
padding: 5px;
border: solid 1px <?php echo $other; ?>;
}

#content
{
padding: 15px;
width: 670px;
margin-top: 10px;
}

section
{

padding: 10px;
float: right;
width: 500px; 
margin-top: 5px;
}

#cover
{
float: right;
height: 300px;
}

#info
{
float: left;
border: solid 1px <?php echo $other; ?>;
width: 370px;
height: 298px;
column-count: 2;
}

.bigger
{
font-size: 1.1em;
}

}

@media screen and (max-width: 699px){

body 
{
width: 99%;
margin: auto;
text-align: center;
}

hr
{
margin-bottom: 20px;
}

header
{
height: 160px;
margin-bottom: 20px;
padding-bottom: 10px;
text-align: center;

}

h1
{

font-size: 1.5em;
}

.logo 
{
background-size: 100px;
width: 100px;
height: 100px;
margin-left: auto;
margin-right: auto;


border-bottom: 1px solid;

}

.logo:hover 
{
transform: scale(1.2);
}

nav
{
padding: 5px;
}

nav ul
{
padding-left: 0px;
list-style: none;
font-size: 1.2em;

}

nav ul li 
{
padding: 3px;
display: inline-block;
}

#cover
{
height: 300px;
}

#info
{
border: solid 1px;
margin-top: 15px;
}

#info ul
{
list-style: none;
padding-left: 0px;
}

section
{
padding: 10px;
}


.bigger
{
font-size: 1.3em;
}

#mainpagebox
{
font-size: 1em;
}

}

